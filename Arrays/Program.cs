﻿using System;
using System.Collections.Generic;//List, Dictionary
using System.Linq; //Cast

namespace Arrays {
    class Program {
        static void Main(string[] args) {

            // double mydouble = 7.76;
            // int myint = (int)mydouble;
            // Console.WriteLine(myint);
            // Console.WriteLine(mydouble);
            // string mystring = mydouble.ToString();

            // var jag = new int[][] {
            //     new int[]{4,5},
            //     new int[]{4,5},
            //     new int[]{4,5}
            // };
            // foreach (var each in jag) Console.WriteLine(string.Join(", ", each));
            // Console.WriteLine(jag[0][1]);

            // var jag2 = new int[][,] {
            //     new int[,] { {3,4}, {2,3}, {9,8} },
            //     new int[,] { {3,4}, {2,3} },
            //     new int[,] { {3,4}, {2,3}, {9,8}, {22,34} },
            // };

            // foreach (var each in jag2) {
            //     foreach (var each2 in each) {
            //         Console.WriteLine(string.Join(", ", each2));
            //     }
            // } 

            // Console.WriteLine(jag2[0][2,0]);


            // var arr2d = new int[,]{ //you could put int[3,2] but type inference has you covered
            //     {5,4},
            //     {3,7},
            //     {8,9}
            // };

            // for (int i = 0; i < arr2d.GetLength(0); i++) { //this gets the length of the type int[3,2]
            //     for (int j = 0; j < arr2d.GetLength(1); j++) {
            //         Console.Write(arr2d[i,j] + "\t");
            //     }
            //     Console.WriteLine();
            // }

            // Console.WriteLine(String.Join(" ", arr2d.Cast<int>()));

            // foreach (var each in arr2d) {
            //     Console.WriteLine(string.Join(", ", each));
            // }

            // var arrEmpty3d = new int[3,3,3];//27 zeros
            // Console.WriteLine(arrEmpty3d.Length);
            // foreach (var each in arrEmpty3d) {
            //     Console.Write("{0} ", each);
            // }


            // Console.WriteLine(string.Join(", ", args));

            // var dic = new Dictionary<int,string>() {
            //     {4, "four"},
            //     {3, "three"},
            //     {6, "six"}
            // };
            // dic.Add(5, "five");
            // Console.WriteLine(dic);
            // Console.WriteLine(string.Join(", ", dic));

            // foreach (KeyValuePair<int,string> each in dic) {
            //     Console.WriteLine(each);
            // }
            
            

            // var lis = new List<int>{5,4,3,2,2};
            // lis.Insert(2, 55);
            // lis.RemoveAt(2);
            // lis.Add(3);
            // lis.AddRange(new List<int>{3,3,4,5});
            // Console.WriteLine(string.Join(", ", lis));

            // var lis = new List<int>{5,4,3,2,2};
            // var arr = new int[]{3,4,5,6};
            // lis.AddRange(arr);
            // Console.WriteLine(string.Join(", ", lis));
        }
    }
}
