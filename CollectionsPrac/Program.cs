﻿using System;
using System.Collections.Generic;

namespace CollectionsPrac
{
    class Program
    {
        static void Main(string[] args) {

            var numArr = new int[]{0,1,2,3,4,5,6,7,8,9};
            var strArr = new string[]{"Tim", "Martin", "Nikki", "Sara"};
            var boolArr = new bool[10]{true, false, true, false, true, false, true, false, true, false};
            // Console.WriteLine(string.Join(", ", numArr));
            // Console.WriteLine(string.Join(", ", strArr));
            // Console.WriteLine(string.Join(", ", boolArr));


            var icecreamLis = new List<string>{"Chocolate", "Vanilla", "Strawberry", "Blackberry", "Cookie Dough"};
            // Console.WriteLine(string.Join(", ", icecreamLis));
            // Console.WriteLine(icecreamLis.Count);
            // Console.WriteLine(icecreamLis[2]);
            icecreamLis.RemoveAt(2);
            // Console.WriteLine(string.Join(", ", icecreamLis));
            // Console.WriteLine(icecreamLis.Count);


            var dic = new Dictionary<string, string>();
            for (var i = 0; i < strArr.Length; i++) {
                dic.Add(strArr[i], icecreamLis[i]);
            }
            Console.WriteLine(string.Join(", ", dic));

        }
    }
}

            // // Console.WriteLine(string.Join(", ", args));

            // var dic = new Dictionary<int,string>() {
            //     {4, "four"},
            //     {3, "three"},
            //     {6, "six"}
            // };
            // dic.Add(5, "five");
            // Console.WriteLine(dic);
            // Console.WriteLine(string.Join(", ", dic));

            // foreach (KeyValuePair<int,string> each in dic) {
            //     Console.WriteLine(each);
            // }
            
            

            // // var lis = new List<int>{5,4,3,2,2};
            // // lis.Insert(2, 55);
            // // lis.RemoveAt(2);
            // // lis.Add(3);
            // // lis.AddRange(new List<int>{3,3,4,5});
            // // Console.WriteLine(string.Join(", ", lis));

            // // var lis = new List<int>{5,4,3,2,2};
            // // var arr = new int[]{3,4,5,6};
            // // lis.AddRange(arr);
            // // Console.WriteLine(string.Join(", ", lis));