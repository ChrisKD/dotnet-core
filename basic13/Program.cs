﻿using System;
using System.Collections.Generic;

namespace basic13
{
    class Program
    {
        static void Main(string[] args) {
            var arr = new int[]{8,7,5,3,2,2,9,2};
            var arr2 = new int[]{8,7,5,3,2,2,9,2,-4};

            //13.)
            Console.WriteLine(string.Join(", ", NumToString(arr2)));

            // //12.)
            // Console.WriteLine(string.Join(", ", ShiftValues(arr2)));

            //11.)
            // MaxMinAvg(arr);

            // //10.)
            // Console.WriteLine(string.Join(", ", EliminateNegatives(arr2)));

            // //9.)
            // Console.WriteLine(string.Join(", ", SquareArrayValues(arr)));

            // //8.)
            // Console.WriteLine(GreaterThanY(arr, 3));

            // //7.)
            // Console.WriteLine(string.Join(", ", OddArray()));

            // //6.)
            // Console.WriteLine(GetAverage(arr));

            // //5.)
            // Console.WriteLine(FindMax(arr));

            // //4.) iterate through an array and print each value
            // var arr = new int[]{3,4,5,6,7};
            // Console.WriteLine(string.Join(", ", arr));

            // //3.) print all nums and sum from 1-255
            // var total = 0;
            // for (var i=1; i<256; i++) {
            //     total = total + i;
            //     Console.WriteLine($"{i} {total}");
            // }

            // //2.) print odd numbers between 1-255
            // for (var i=1; i<256; i+=2) {
            //     Console.WriteLine(i);
            // }

            // //1.) print 1-255
            // for (var i=1; i<256; i++) {
            //     Console.WriteLine(i);
            // }
        }

        //13.) Num to String
        public static object[] NumToString(int[] arr) {
            var objArr = new object[arr.Length];
            for (var i = 0; i < arr.Length; i++) {
                if (arr[i] < 0) {
                    objArr[i] = "Dojo";
                } else {
                    objArr[i] = arr[i];
                }
            }
            return objArr;
        }

        //12.) Shifting the values in the array
        public static int[] ShiftValues(int[] arr) {
            for (var i=0; i<arr.Length-1; i++) {
                arr[i] = arr[i+1];
            }
            arr[arr.Length-1] = 0;
            return arr;
        }

        //11.) print Max, Min, Avg
        public static void MaxMinAvg(int[] arr) {
            var max = arr[0];
            var min = arr[0];
            float total = 0;
            for (var i=0; i<arr.Length; i++) {
                if (arr[i] > max) {
                    max = arr[i];
                }
                if (arr[i] < min) {
                    min = arr[i];
                }
                total = total + arr[i];
            }
            float avg = total/arr.Length;
            Console.WriteLine($"{max}, {min}, {avg}");
        }

        //10.) Eliminate Negative Numbers
        public static int[] EliminateNegatives(int[] arr) {
            for (int i = 0; i<arr.Length; i++) {
                if (arr[i] < 0) {
                    arr[i] = 0;
                }
            }
            return arr;
        }

        //9.) Square Array Values
        public static int[] SquareArrayValues(int[] arr) {
            var newArr = new int[arr.Length];
            for (var i = 0; i<arr.Length; i++) {
                newArr[i] = (int)Math.Pow(arr[i], 2);
            }
            return newArr;
        }

        //8.) Greater than Y
        public static int GreaterThanY(int[] arr, int num) {
            int total = 0;
            foreach (var each in arr) {
                if (each > num) {
                    total++;
                }
            }
            return total;
        }

        //7.) Array with Odd Numbers
        public static int[] OddArray() {
            var lis = new List<int>();
            for (var i = 1; i < 256; i+=2) {
                lis.Add(i);
            }
            int[] arr = lis.ToArray();
            return arr;
        }

        // 6.) GetAverage
        public static float GetAverage(int[] arr) {
            float total =0;
            foreach (var each in arr) {
                total+=(float)each;
            }
            return total/arr.Length;
        }

        //5.) FindMax
        public static int FindMax(int[] arr) {
            var max = arr[0];
            foreach (var each in arr) {
                if (each > max) {
                    max = each;
                }
            }
            return max;
        }

    }
}

