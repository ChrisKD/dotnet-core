﻿using System;
using System.Collections.Generic;

namespace unboxing
{
    class Program
    {
        static void Main(string[] args) {
            var emptyList = new List<object>();
            // emptyList.AddRange(7,28,-1,true,"chair");
            emptyList.Add(7);
            emptyList.Add(28);
            emptyList.Add(-1);
            emptyList.Add(true);
            emptyList.Add("chair");


            int total = 0;
            string words = "";
            foreach (var each in emptyList) {
                Console.WriteLine(each);
                if (each is int) {
                    total = total + (int)each;
                }
                if (each is string) {
                    words = words + each as string;
                }
            }
            Console.WriteLine(total);
            Console.WriteLine(words);

            int myint = 65;
            double mydouble = myint;

            double mydouble2 = 3.13159;
            int myint2 = (int)mydouble2;

            string mystring = "24";
            int myint3 = int.Parse(mystring);

            Console.WriteLine($"{mydouble}, {myint2}, {myint3}");
            

        }
    }
}
