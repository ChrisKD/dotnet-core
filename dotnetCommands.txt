//To create a new project, or if 
dotnet new console
dotnet new console -o AnotherProject

//To update dependencies if you changed them from Nuget
dotnet restore

//To run the application Project.cs
dotnet run

//will present a list of potential templates you can use
dotnet new

