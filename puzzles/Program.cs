﻿using System;
using System.Collections.Generic;

namespace puzzles
{
    class Program
    {
        static void Main(string[] args) {
            // RandomArray();
            // TossCoin();
            Console.WriteLine(string.Join(", ", Names()));
        }

        public static List<string> Names() {
            var lis = new List<string>{"Todd", "Tiffany", "Charlie", "Geneva", "Sydney"};
            Console.WriteLine(string.Join(", ", lis));
            var rand = new Random();
            for (var i = 0; i<5; i++) {
                var myrand = rand.Next(0,5);
                var temp = lis[i];
                lis[i] = lis[myrand];
                lis[myrand] = temp;
            }
            Console.WriteLine(string.Join(", ", lis));
            for (var i = 4; i> -1; i--) {
                if (lis[i].Length < 6) {
                    lis.RemoveAt(i);
                }
            }
            return lis;
        }

        public static string TossCoin() {
            Console.WriteLine("Tossing a Coin!");
            var rand = new Random();
            var myrand = rand.Next(0,2);
            if (myrand == 0) {
                Console.WriteLine("Heads");
                return "Heads";
            } else {
                Console.WriteLine("Tails");
                return "Tails";
            }

        }

        public static void RandomArray() {
            var rand = new Random();
            var arr = new int[10];
            int min = 5;
            int max = 5;
            int myrand;
            int total = 0;
            for (var i = 0; i<10; i++){
                myrand = rand.Next(5,26);
                arr[i] = myrand;
                if (min > myrand) {
                    min = myrand;
                }
                if (max < myrand) {
                    max = myrand;
                }
                total+=myrand;
            }
            Console.WriteLine($"{min} {max} {total} {(double)total/arr.Length}");
            Console.WriteLine(string.Join(", ", arr));
        }
    }
}
